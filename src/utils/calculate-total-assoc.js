const { 
  add, 
  assoc, 
  compose, 
  prop, 
  reduce,
  ap 
} = require('ramda');

/**
 * total :: { total: Int }
 * 
 * s :: { 
 *  total: Int, 
 *  allowedCoints: [Int],
 *  choices: [{ Choices }]
 * }
 * 
 * calculateTotalAssoc :: total -> s -> s
 */
module.exports = total => s =>
  assoc(
    'total',
    compose(
      reduce(add, 0),
      ap([prop('total')])
    )([total, s])
  )(s)