// isCharacterInAlphabet :: String -> Bool
module.exports = str =>
  str.length === 1 && str.match(/[a-z]/i)