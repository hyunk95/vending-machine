const { compose, equals, head } = require('ramda')

// isHeadPoint :: String -> Bool
module.exports =
  compose(
    equals('.'),
    head
  )