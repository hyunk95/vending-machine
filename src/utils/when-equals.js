const { when, equals, always } = require('ramda')

module.exports = propName => fn =>
  when(
    equals(propName),
    always(fn)
  )