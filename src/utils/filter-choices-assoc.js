const { 
  assoc, 
  compose, 
  equals, 
  prop, 
  filter 
} = require('ramda');

const CHOICES = require('../constants/choices');

/**
 * filterChoicesAssoc :: s -> s
 */

module.exports = s =>
  assoc(
    'choices',
    filter(
      compose(
        equals(
          prop('total', s)
        ),
        prop('price')
      ),
      CHOICES
    ),
    s
  )