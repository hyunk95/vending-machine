const { 
  add, 
  assoc, 
  compose,  
  __, 
  prop, 
  filter, 
  lte, 
  pluck, 
  length, 
  gt 
} = require('ramda');

const COINS = require('../constants/coins')
const CHOICES = require('../constants/choices')

/**
 * filterChoicesAssoc :: s -> s
 */

module.exports = s =>
  assoc(
    'allowedCoins',
    filter(
      coin =>
        compose(
          gt(__, 0),
          length,
          filter(
            lte(
              compose(
                add(coin),
                prop('total')
              )(s),
            )
          )
        )(pluck('price', CHOICES)),
      COINS
    ),
    s
  )