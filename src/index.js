const readline = require('readline')
const util = require('util')
const colors = require('colors')
const { findIndex, equals, compose, prop, join, mapObjIndexed, values, path } = require('ramda')

const CHOICES = require('./constants/choices')
const isHeadPoint = require('./utils/predicates/is-head-point')
const isCharacterInAlphabet = require('./utils/predicates/is-character-in-alphabet')
const vendingMachineState = require('./vending-machine-state')

const rl = readline.createInterface(
  process.stdin, 
  process.stdout, 
  completer
)

// [string]
const help = [ colors.grey('.coins        ' + 'available coins to insert.')
             ,colors.grey('.choices       ' + 'purchases you can make')
             ,colors.grey('.q[uit]      ' + 'exit console.')
             ].join('\n')

// { CHOICES }
const menu = {
  a: CHOICES[0],
  b: CHOICES[1],
  c: CHOICES[2]
}

var state = Object.freeze(vendingMachineState(0)(null));

// menuString :: { CHOICES } -> String
const menuString = 
  compose(
    join('\n'),
    values,
    mapObjIndexed(
      (num, key, obj) =>
       `[${key}] ${obj[key].name}: $${obj[key].price / 100}`,
    ),
  )(menu)

// impure function
function completer(line) {
  var completions = '.coins .choice .quit .q'.split(' ')
  var hits = completions.filter(function(c) {
    if (c.indexOf(line) == 0) {
      return c;
    }
  });
  return [hits && hits.length ? hits : completions, line];
}

// impure function
function welcome() {
  console.log([ "VENDING MACHINE"
            , "= Welcome, enter .help if you're lost."
            ].join('\n').grey);

  console.log(menuString.yellow)
  console.log('Enter amount in cents'.grey)

  prompt();
}

// impure function
function prompt() {
  var arrow    = '> '
    , length = arrow.length
    ;

  rl.setPrompt(colors.grey(arrow), length);
  rl.prompt();
}

// impure function
function exec(command) {
  let int = parseInt(command)

  if(isHeadPoint(command)) {
    let action = command.slice(1)

    switch(action) {
      case 'help':
        console.log(help.yellow);
        break;
      case 'coins':
        console.log(state.allowedCoins, 'Coins you are allowed to use');
        break;
      case 'choices':
        console.log(state.choices, 'Things you can buy right now');
        break;
      case 'exit':
        break;
      case 'q':
        process.exit(0);
        break;
    }
  }

  if(!isNaN(int)) {
    let idx = findIndex(equals(int), state.allowedCoins) 

    if (idx != -1) {
      state = vendingMachineState(int)(state)
    } else {
      console.log('Invalid Number'.red)
    }
  }

  if(isCharacterInAlphabet(command)) {
    let name = path([command, 'name'], menu)
    let price = path([command, 'price'], menu)

    let idx = findIndex(compose(
      equals(name),
      prop('name') 
    ), state.choices)

    if (idx != -1) {
      console.log(`You purchased a ${name}`.green)
      state = vendingMachineState(
        -Math.abs(price)
      )(state)
    } else {
      console.log('Invalid Amount'.red)
    }
  }

  console.log(`Total: $${state.total/100}`)
  console.log(menuString)

  prompt();
}

rl.on('line', function(cmd) {
  exec(cmd.trim());
}).on('close', function() {
  util.puts('goodbye!'.green);
  process.exit(0);
});

welcome();