/**
  {
    total: Int,
    allowedCoins: [Int],
    choices: [{
      name: String,
      price: Int
    }]
  }
*/

module.exports = { 
  total: 0, 
  allowedCoins: [10, 20, 50, 100, 200],
  choices: [] 
}