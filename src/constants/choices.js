/**
 [Choices]

 {
   name: String,
   price: Int
 }
 */

const CHOICES = [{
  name: 'Caramel',
  price: 250
}, {
  name: 'Hazelnut',
  price: 310
}, {
  name: 'Organic Raw',
  price: 200
}]

module.exports = CHOICES;