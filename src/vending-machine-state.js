const State = require('./types/state');
const { compose, chain, objOf, always } = require('ramda');

const filterCoinsAssoc = require('./utils/filter-coins-assoc')
const calculateTotalAssoc = require('./utils/calculate-total-assoc')
const filterChoicesAssoc = require('./utils/filter-choices-assoc')
const initalState = require('./constants/initial-state')

const modify = State.modify;

/**
  s :: {
    total: Int,
    allowedCoins: [Int],
    choices: [{
      name: String,
      price: Int
    }]
  }
*/

// State (Tuple2 s s) -> State (Tuple2 null s)
const calculateAllowedCoins =
  modify(filterCoinsAssoc)

// State (Tuple2 s s) -> State (Tuple2 null s)
const updateTotal = total =>
  modify(calculateTotalAssoc(total))

// State (Tuple2 s s) -> State (Tuple2 null s)
const calculateChoices =
  modify(filterChoicesAssoc)

// calculateTotal :: Int -> s -> State (Tuple2 null s)
const calculateTotal =
  compose(
    chain(always(calculateAllowedCoins)),
    chain(always(calculateChoices)),
    chain(updateTotal),
    State.of,
    objOf('total')
  )

// vendingMachineState :: Int -> s | null -> s
module.exports = value => obj => {
  let state = obj || initalState

  return calculateTotal(value).exec(state)
}