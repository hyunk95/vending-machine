"use strict";
const chai = require("chai");
const { expect } = require("chai");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const { path, prop } = require('ramda');

const vendingMachineState = require('../src/vending-machine-state');
const initialState = require('../src/constants/initial-state');

chai.should();
chai.use(sinonChai);

describe('Vending Machine State', () => {
  let state, newState, value

  beforeEach('set state', () => {
    state = vendingMachineState(0)(null)
  })

  context('when initial state', () => {
    it('should equal the initial state when passed null arg', () => {
      expect(state).to.eql(initialState)
    })
  })

  context('when provided a value', () => {
    beforeEach('set value', () => {
      value = 20
    })

    beforeEach('invoke function', () => {
      newState = vendingMachineState(value)(state)
    })

    it('should update the state', () => {
      expect(
        prop('total', newState) - prop('total', state)
      ).to.eql(value)
    })
  })

  context('when total is equal to a price', () => {
    beforeEach('set value', () => {
      value = 310
    })

    beforeEach('set state', () => {
      state = vendingMachineState(value)(state)
    })

    it('should have Hazelnut as an offer', () => {
      expect(path(['choices', 0, 'name'], state)).to.eql('Hazelnut')
    })
  })

  context('when total is less than price', () => {
    beforeEach('set value', () => {
      value = 290
    })

    beforeEach('set state', () => {
      state = vendingMachineState(value)(state)
    })

    it('should only list coins that will sum to be less than the prices of the items', () => {
      expect(path(['allowedCoins'], state)).to.eql([
        10, 20
      ])
    })
  })
})